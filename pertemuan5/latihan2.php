<?php
//pengulangan pada array
//for/
//foreach/
$angka=[1,2,4,5,12,10,7,10,11,28];
?>

<html>
<head>
    <title>Latihan array</title>
    <style>
        .kotak {
            width: 50px;
            height: 50px;
            background-color: salmon;
            text-align: center;
            line-height: 50px;
            margin: 3px;
            float: left;
        }

        .clear {clear: both;}
    </style>
</head>
<body>
    <!-- <?php for($i=0; $i< count($angka); $i++) {?>
        <div><?php echo $angka[$i];?></div>
    <?php }?> -->

    <!-- <?php for ($i=0; $i< count($angka); $i++) { ?>
        <div><?php echo $angka[$i]; ?></div>
    <?php } ?> -->

    <!-- <?php for ($i=0; $i< count($angka); $i++) : ?>
        <div><?= $angka[$i]; ?></div>
    <?php endfor ?> -->

    <!-- <?php for($i=0; $i< count($angka); $i++) : ?>
        <div><?= $angka[$i]; ?></div>
    <?php endfor ?> -->

    <?php for($i=0; $i< count($angka); $i++) : ?>
        <div class="kotak"><?= $angka[$i]; ?></div>
    <?php endfor ?>

    <div class=clear></div>

    <!-- <?php foreach($angka as $a) : ?>
        <div class="kotak"><?= $a ?></div>
    <?php endforeach ?> -->

    <?php foreach ($angka as $a) { ?>
        <div class="kotak"><?php echo $a ?></div>
    <?php } ?>

    <div class="clear"></div>

    <?php foreach($angka as $b) : ?>
        <div class="kotak"><?= $b ?></div>
    <?php endforeach ?>
</body>
</html>