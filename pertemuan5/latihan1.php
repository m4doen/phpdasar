<?php
//array
//variabel yang dapat memiliki banyak nilai

$hari = array("Senin", "Selasa", "Rabu");

$bulan = ["Januari", "Februari", "Maret"];

$arr1 = [123, "tulisan", false];


//menampilkan array dapat menggunakan print_r atau var_dump
// var_dump($hari);
// echo "<br>";
// print_r($bulan);
// echo "<br>";
// echo $arr1[0];
// echo "<br>";
// echo $bulan[1];

var_dump($hari);
$hari[]="Kamis";
$hari[]="Jum'at"; "Sabtu";
echo "<br>";
var_dump($hari);
?>