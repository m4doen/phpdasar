<?php
//pengulangan
//for (inisialisasi(variabel awal untuk pengulangan), kondisi terminasi(variabel pengehentian), increment/decrement(variabel untuk maju atau mundur))
//for (I,T,In/De){syntax/bagian yang akan diulang}
//array dalam php, dimulai dari 0
//array biasanya menggunakan variabel i
//while (inisialisasi, while, {bagian yg akan diulang}, terminasi, increment/decrement)
//do..while (inisialisasi, do, {bagian yang akan diulang}, increment/decrement, terminasi)
//foreach

// for($i=0;$i<5;$i++){
//     echo "Hi!! <br>";
// }

// $i = 0;
// while ($i < 6){
//     echo "tes kedua while<br>";
//     $i++;
// }

// $i = 0;
// do {
//     echo "tes<br>";
//     $i++;
// } while ($i < 5);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Latihan 1</title>
    <style>
        .warna-baris{
            background-color:silver;
        }
    </style>
</head>
<!-- buat tabel 3 baris 5 kolom dengan properties tabel, border 1, cellpadding 10,
cellspacing 0, dengan isi 1,1 hingga 3,5 pada akhir tabel-->
<body>
   <!--<table border="2", cellpadding="10", cellspacing="0">
        <?php
            // for($i=1; $i<=5; $i++){
            //     echo "<tr>";
            //     for ($j=1; $j<=10; $j++){
            //         echo "<td>$i,$j</td>";
            //     }
            //     echo "</tr>";
            // }
        ?>
   </table>-->
   <table border="3", cellpadding="10", cellspacing="0">
        <?php for($i=1; $i<=10; $i++) : ?>
            <!-- kurang kurawa awal '{' dapat diganti menjadi titik dua, 
                kurung kurawa akhir '}' dapat diganti endfor, endif, endforeach-->
            <?php if($i % 2 == 0) :?>
            <tr class="warna-baris">
            <?php else : ?>
            <?php endif ; ?>
                <?php for($j=1; $j<=5; $j++) : ?>
                    <td><?= "$i,$j";?></td>
                    <!--  'php echo' dapat diganti menjadi '=' -->
                <?php endfor; ?>
            </tr>
        <?php endfor; ?>
   </table>
</body>
</html>
