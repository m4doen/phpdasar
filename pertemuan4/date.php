<?php

// parameter "l" adalah untuk menampilkan hari
// d = tanggal, M = bulan dalam text, Y = tahun dalam angka
// echo date ("l, d-M-Y");

//unix timestamp/epoch time
//detik yang sudah berjalan sejak 1 januari 1970
// echo date("l", time()+60*60*24*2);

//mktime (jam, menit, detik, bulan, tanggal, tahun)

// echo date("l", mktime(0,0,0,02,11,1991));

// echo date("l", strtotime("11 feb 1991"));

//string
//string, strlen (mengukur panjang string)
//strcmp, untuk perbandingan
//explode, untuk memecah string menjadi array
//htmlspecialchars, untuk mencegah dari hacker

//var_dump(), untuk mencetak isi dari sebuah variabel, array, objeck
//isset(), untuk mengecek variabel sudah pernah dibuat atau belum
//menghasilkan nilai boolean
//empty(), untuk mengecek variabel tersebut kosong atau tidak
//die(), program di bawah tidak akan dieksekusi
//sleep(), menjeda program selama 2 detik

//