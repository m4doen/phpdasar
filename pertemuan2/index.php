<?php
//syntax php
//standar output

//echo, print
//echo, untuk mencetak string, bisa menggunakan kutip dua (") atau kutip satu (')
//menggunakan kutip atau dua atau satu, didasarkan kepada karakter yang berada dalam kutip tersebut
//kutip dua dapat mengecek interpolasi(ada atau tidaknya variabel)
//jika kata tersebut ada tanda kutip satu seperti kata jum'at, maka menggunakan kutip dua

// print_r,, untuk menampilkan array
//var_dump,, untuk menampilkan variabel, fungsinya untuk mencari error atau debugging

//variabel dan tipe data
//variabel digunakan untuk menampung sebuah nilai

//concanation, penggabungan string, operatornya titik (.)

// $nama_depan = "Sammy";
// $nama_belakang = "Lugina";
// echo $nama_depan . " " . $nama_belakang;

// $nama = "sammy";
// $nama .= " ";
// $nama .= "lugina";
// echo $nama;

//perbandingan, < > <= >= == !=
// var_dump (1=="1");

//operator identitas ===(sama dengan), !==(tidak sama dengan)
// var_dump(1==="1");

//logika, && (dan), || (or), ! (tidak, bukan)
$x = 30;
var_dump($x < 10 && $x % 2 == 0);