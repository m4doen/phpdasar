<?php 

require 'function.php';

$tesh = query("SELECT * FROM teshapus");

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>teshapus</title>
</head>
<body>
    <h1>Teshapus</h1>
    <a href="tambah.php">Tambah Data Tes</a>
    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>Nama</th>
            <th>Email</th>
        </tr>
        <?php $i = 1; ?>
        <?php foreach($tesh as $tes) : ?>
        <tr>
            <td><?= $i; ?></td>
            <td>
                <a href="">Ubah</a> |
                <a href="hapus.php?id=<?= $tes['id']; ?>">Hapus</a>
            </td>
            <td><?= $tes["nama"]; ?></td>
            <td><?= $tes["email"]; ?></td>
        </tr>
        <?php $i++; ?>
        <?php endforeach; ?>
    </table>
</body>
</html>