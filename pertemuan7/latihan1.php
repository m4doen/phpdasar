<?php 
//variable scope / lingkup variabel

// $i= 10;

// function tampil_i (){
//     $i= 20;
//     echo $i;
// }

// tampil_i();

// echo "<br>";

// echo $i;

//dalam layar, tampil 20 dan 10, karena $i di awal memiliki scope global,
//sedangkan $i dalam function tampil_i memiliki scope lokal

// $i = 10;
// function tampil_i(){
//     global $i;
//     echo $i;
// }
//  tampil_i();

//php memiliki variabel super global: (memiliki sifat array associative)
//$_GET= mengambil data dari url
//$_POST
//$_REQUEST
//$_SESSION
//$_COOKIE
//$_SERVER
//$_ENV

$mahasiswa=
        [
            [
                "nama" => "Sammy Lugina",
                "nrp" => "123456",
                "jurusan" => "Manajemen",
                "email" => "m4doen@gmail.com",
                "gambar" => "man.jpg"
            ],
            [
                "nama" => "Muflikha Mayazi",
                "nrp" => "123457",
                "jurusan" => "Kedokteran dan Pendidikan Dokter",
                "email" => "muflikha.ayaz@gmail.com",
                "gambar" => "woman.jpg"
            ],
            [
                "nama" => "Toni Surya Atmadja",
                "nrp" => "123458",
                "jurusan" => "Ilmu Komputer",
                "email" => "teu.nyaho@gmail.com",
                "gambar" => "man.jpg"
            ]
        ]; 

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GET</title>
</head>
<body>
    <h1>Daftar Mahasiswa</h1>
    <ul>
        <?php foreach($mahasiswa as $m) : ?>
           <li>
                <a href="latihan2.php?nama=<?=$m["nama"]?>&nrp=<?=$m["nrp"]?>&email=<?=$m["email"]?>&jurusan=<?=$m["jurusan"]?>&gambar=<?=$m["gambar"]?>"><?= $m["nama"] ?></a>
           </li>
        <?php endforeach ?>
    </ul>
</body>
</html>