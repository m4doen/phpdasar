<?php 

$conn = mysqli_connect("localhost", "username", "password", "database_name");

// memeriksa koneksi
if (!$conn) {
    die("Koneksi gagal: " . mysqli_connect_error());
}

// memeriksa apakah form telah disubmit
if (isset($_POST['submit'])) {
    // mengambil nilai dari input field
    $id = $_POST['id'];
    
    // menghapus data dari tabel tertentu
    $sql = "DELETE FROM nama_tabel WHERE id = $id";

    if (mysqli_query($conn, $sql)) {
        echo "Data berhasil dihapus";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

// menutup koneksi
mysqli_close($conn);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Hapus Data</title>
</head>
<body>
	<h1>Hapus Data</h1>
	<form method="post" action="hapus_data.php">
		<label for="id">ID:</label>
		<input type="text" name="id" id="id">
		<input type="submit" name="submit" value="Hapus">
	</form>
</body>
</html>