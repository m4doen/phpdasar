<?php

require "function2.php";

$mahasiswa = query("SELECT * FROM mahasiswa");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tes sendiri</title>
</head>
<body>
    <h1>DAFTAR MAHASISWA</h1>
    <a href="tambah2.php">Tambah Data Mahasiswa</a>
    <br><br>
    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>Gambar</th>
            <th>NRP</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Jurusan</th>
        </tr>
        <?php $i=1; ?>
        <?php foreach ($mahasiswa as $tes) : ?>
        <tr>
            <td><?= $i; ?></td>
            <td>
                <a href="">ubah</a> |
                <a href="hapus.php?id=<?= $tes['id']; ?>">hapus</a>
            </td>
            <td><img src="img/<?= $tes["gambar"]; ?>" alt=""></td>
            <td><?= $tes["nrp"]; ?></td>
            <td><?= $tes["nama"]; ?></td>
            <td><?= $tes["email"]; ?></td>
            <td><?= $tes["jurusan"]; ?></td>
        </tr>
        <?php $i++ ; ?>
        <?php endforeach; ?>
    </table>
</body>
</html>