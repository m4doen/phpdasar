<!DOCTYPE html>
<head>
    <title>Associative Array</title>
</head>
<body>
    <?php
        $mahasiswa=
        [
            [
                "nama" => "Sammy Lugina",
                "nrp" => "123456",
                "jurusan" => "Manajemen",
                "email" => "m4doen@gmail.com",
                "gambar" => "man.jpg"
            ],
            [
                "nama" => "Muflikha Mayazi",
                "nrp" => "123457",
                "jurusan" => "Kedokteran dan Pendidikan Dokter",
                "email" => "muflikha.ayaz@gmail.com",
                "gambar" => "woman.jpg"
            ],
            [
                "nama" => "Toni",
                "nrp" => "123458",
                "jurusan" => "Ilmu Komputer",
                "email" => "teu.nyaho@gmail.com",
                "gambar" => "man.jpg"
            ]
        ]; 
    { ?>

    <h1>Daftar Mahasiswa</h1>
        <?php foreach ($mahasiswa as $m) : ?>
            <ul>
                <li><img src="img/<?= $m["gambar"]; ?>"></li>
                <li><?= $m["nama"]; ?></li>
                <li><?= $m["nrp"]; ?></li>
                <li><?= $m["email"]; ?></li>
                <li><?= $m["jurusan"]; ?></li>
            </ul>
        <?php endforeach ?>

    <?php } ?>
</body>
</html>